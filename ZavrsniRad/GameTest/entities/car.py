import pyglet
from pyglet.sprite import Sprite
import config
from math import cos, sin, radians
from system.util import distance

ACCELERATE_BY = 0.1

class Car(pyglet.sprite.Sprite):

    def __init__(self, *args, **kwargs):
        super(Car, self).__init__(*args, **kwargs)

        self.speed = 5
        self.x_direction = 0
        self.y_direction = 1
        self.collision_distance = self.image.height/2
        self.begin_x = 200
        self.begin_y = 200
        self.begin_rot = 0

    def rot(self, degree):
        if self.speed:
            self.update(rotation=(degree + self.rotation) % 360)
            self.y_direction = cos(radians(degree + self.rotation))
            self.x_direction = sin(radians(degree + self.rotation))

    def step(self, action):
        if action == 0:
            self.rot(-6)
        if action == 1:
            self.rot(6)
            
    def update_self(self):
            
        self.x += (self.speed * self.x_direction)
        self.y += (self.speed * self.y_direction)

        if self.speed > 12:
            self.speed = 12

    def accelerate(self):
        self.speed += ACCELERATE_BY

        if self.speed > 12:
            self.speed = 12
        

    def decelerate(self):
        self.speed -= ACCELERATE_BY * 3

        if self.speed <= 0:
            self.speed = 0

    def handbrake(self):
        self.speed -= ACCELERATE_BY

        if self.speed < 0:
            self.speed = 0

    def draw_self(self):
        self.draw()

    def collides_with(self, x, y):
        actual_distance = distance(self.position, (x,y))
        return (actual_distance <= self.collision_distance)

    def reset(self):
        self.update(x = self.begin_x, y = self.begin_y, rotation = self.begin_rot)
        self.speed = 4
        self.y_direction = cos(radians(self.begin_rot))
        self.x_direction = sin(radians(self.begin_rot))