import pyglet
import config
from entities.car import Car
from entities.DQN_agent import DQNAgent
from pyglet.window import key
from pyglet.gl import *
import numpy as np
from math import sin, cos, radians, sqrt
import random
import pymunk
from pymunk.pyglet_util import DrawOptions
import os

CHECKPOINT_REWARD = 100
CRASH_PENALTY = 300
LAP_REWARD = 500
TIME_REWARD = 1/120
AGGREGATE_STATS_EVERY = 10
MIN_REWARD = 500
MAX_STEPS = 2000
MODEL_NAME = "1st"

epsilon = 0.8 
EPSILON_DECAY = 0.99975
MIN_EPSILON = 0.001

assets_path = os.path.join(os.getcwd(), "assets")
pyglet.resource.path = [assets_path]

window = pyglet.window.Window(height=config.window_height,
                              width=config.window_width, 
                              resizable = True,
                              vsync=True)

player = Car(pyglet.resource.image('car.png'), x = 200, y = 200,  subpixel = True)
player.image.anchor_x = player.image.width / 2
player.image.anchor_y = player.image.height / 2
player = Car(pyglet.resource.image('car.png'), x = 200, y = 200,  subpixel = True)

score_label = pyglet.text.Label(text="Score: 0", y = config.window_height - 25, x = 2)
sensor_label = pyglet.text.Label(text="Distances: ", y = config.window_height - 50, x = 2, multiline = True, width = 500)

options = DrawOptions()
options.flags = pymunk.SpaceDebugDrawOptions.DRAW_SHAPES
options.flags |= pymunk.SpaceDebugDrawOptions.DRAW_COLLISION_POINTS

start = 0
mode = 0
cp_count = 0
lap = 0
episode = 1
episode_reward = 0
ep_rewards = []
reward = 0
step = 1
load = False
first = True
out_count = 0
in_count = 0

sensor = pyglet.graphics.vertex_list(0, ('v2f'))
x_sensors = []
y_sensors = []
sensor_length = 10000

checkpoints = pyglet.graphics.vertex_list(0, ('v2f'))
checkpoints_x_y = []

x_out = []
y_out = []
x_in = []
y_in = []

space = pymunk.Space()
space.debug_draw(options)

pressed_keys = []

sensor_distances = [0, 0, 0, 0, 0, 0]

actions = ['Left', 'Right', 'Nothing']

agent = DQNAgent()

def draw():
	global cp_count
	global checkpoints
	global lap
	global epsilon
	global episode_reward
	global reward
	global step
	global episode

	done = False

	x_sensors = [player.x - cos(radians(player.rotation)) * sensor_length, 
	      player.x - cos(radians(player.rotation + 45)) * sensor_length,
	      player.x + sin(radians(player.rotation)) * sensor_length,
	      player.x + sin(radians(player.rotation + 45)) * sensor_length,
	      player.x + cos(radians(player.rotation)) * sensor_length]

	y_sensors = [player.y + sin(radians(player.rotation)) * sensor_length, 
	      player.y + sin(radians(player.rotation + 45)) * sensor_length, 
	      player.y + cos(radians(player.rotation)) * sensor_length,
	      player.y + cos(radians(player.rotation + 45)) * sensor_length,
	      player.y - sin(radians(player.rotation)) * sensor_length]

	x1 = player.x
	y1 = player.y

	for i in range(5):
			segment_q = space.segment_query_first((player.position), (x_sensors[i], y_sensors[i]), 1, pymunk.ShapeFilter())

			if segment_q != None:
				x_sensors[i] = int(segment_q.point[0])
				y_sensors[i] = int(segment_q.point[1])
			sensor_distances[i] = round(sqrt((x_sensors[i] - x1)**2 + (y_sensors[i] - y1)**2) - player.image.height / 2 + 2, 1)
			senzor = pyglet.graphics.vertex_list(2,
					('v2f', (x1, y1, x_sensors[i], y_sensors[i])),
					('c3B', (0, 0, 255, 255, 0, 0))
					)
			senzor.draw(GL_LINES)
	sensor_distances[5] = round(player.rotation, 1)

	if int(len(checkpoints_x_y)) >=4:
		checkpoints = pyglet.graphics.vertex_list(2,
		('v2f', (checkpoints_x_y[cp_count], checkpoints_x_y[cp_count + 1], checkpoints_x_y[cp_count + 2], checkpoints_x_y[cp_count + 3])),
		('c3B', (0, 255, 0, 0, 255, 0))
		)


	current_state = np.array(sensor_distances)

	checkpoints.draw(GL_LINES)
	score_label.draw()
	sensor_label.draw()

	if isinstance(player, Car):
	    player.draw_self()

	if start:
		if np.random.random() > epsilon:
			action = np.argmin(agent.get_qs(current_state))
		else:
			action = np.random.randint(0, 3)


		player.step(action)

		for i in range(5):
			segment_q = space.segment_query_first((player.position), (x_sensors[i], y_sensors[i]), 1, pymunk.ShapeFilter())

			if segment_q != None:
				x_sensors[i] = int(segment_q.point[0])
				y_sensors[i] = int(segment_q.point[1])
			sensor_distances[i] = round(sqrt((x_sensors[i] - x1)**2 + (y_sensors[i] - y1)**2) - player.image.height / 2 + 2, 1)
			senzor = pyglet.graphics.vertex_list(2,
					('v2f', (x1, y1, x_sensors[i], y_sensors[i])),
					('c3B', (0, 0, 255, 255, 0, 0))
					)
			senzor.draw(GL_LINES)
		sensor_distances[5] = round(player.rotation, 1)

		new_state = np.array(sensor_distances)

		reward = 0

		if int(len(checkpoints_x_y)) >= 4:
			x1 = checkpoints_x_y[cp_count]
			y1 = checkpoints_x_y[cp_count + 1]
			x2 = checkpoints_x_y[cp_count + 2]
			y2 = checkpoints_x_y[cp_count + 3]

			for x in range(min(checkpoints_x_y[cp_count], checkpoints_x_y[cp_count + 2]), 
							max(checkpoints_x_y[cp_count], checkpoints_x_y[cp_count + 2])
							):
				y = (y2 - y1) / (x2 - x1) * (x - x1) + y1
				if(player.collides_with(x, y)):
					reward += CHECKPOINT_REWARD
					cp_count = cp_count + 4
					if cp_count >= len(checkpoints_x_y):
						cp_count = 0
						lap += 1
						reward += LAP_REWARD
					break
			checkpoints = pyglet.graphics.vertex_list(2,
				('v2f', (checkpoints_x_y[cp_count], checkpoints_x_y[cp_count + 1], checkpoints_x_y[cp_count + 2], checkpoints_x_y[cp_count + 3])),
				('c3B', (0, 255, 0, 0, 255, 0))
				)

		for i in range(5):
			if sensor_distances[i] <= 0:
				reward -= CRASH_PENALTY
				done = True

		episode_reward += reward

		agent.update_replay_memory((current_state, action, reward, new_state, done))
		agent.train(done, step)

		current_state = new_state
		step += 1

		if done or step >= MAX_STEPS:
			ep_rewards.append(episode_reward)
			if not episode % AGGREGATE_STATS_EVERY or episode == 1:
				average_reward = sum(ep_rewards[-AGGREGATE_STATS_EVERY:])/len(ep_rewards[-AGGREGATE_STATS_EVERY:])
				min_reward = min(ep_rewards[-AGGREGATE_STATS_EVERY:])
				max_reward = max(ep_rewards[-AGGREGATE_STATS_EVERY:])
				agent.tensorboard.update_stats(reward_avg=average_reward, reward_min=min_reward, reward_max=max_reward, epsilon=epsilon)

				if min_reward >= MIN_REWARD:
					agent.model.save(f'models/{MODEL_NAME}__{max_reward:_>7.2f}max_{average_reward:_>7.2f}avg_{min_reward:_>7.2f}min__{int(time.time())}.model')

			if epsilon > MIN_EPSILON:
				epsilon *= EPSILON_DECAY
				epsilon = max(MIN_EPSILON, epsilon)

			player.reset()
			agent.tensorboard.step = episode
			cp_count = 0
			lap = 0
			episode += 1
			episode_reward = 0

			step = 0

def update(time):
	global start
	global cp_count

	score_label.text = "Episode: " + str(episode) + " Mode: " + str(mode) + " Lap: " + str(lap) + " Episode reward: " + str(round(episode_reward, 2))
	sensor_label.text = "Distances: \t" + str(sensor_distances) + "\n\t\t L\t FL\t F\t FR\t R\t\n Epsilon: " + str(epsilon) + "\n Step: " + str(step)

	space.step(time)

	if not start:
	    if key.W in pressed_keys:
	        player.accelerate()
	    if key.S in pressed_keys:
	        player.decelerate()
	    if key.A in pressed_keys:
	        player.rot(-4)
	    if key.D in pressed_keys:
	        player.rot(4)
	    if key.SPACE in pressed_keys:
	        player.handbrake()

	if isinstance(player, Car) and start:
		player.update_self()

@window.event
def on_mouse_press(x, y, button, modifiers):
	if not start:
		global mode
		global checkpoints
		global cp_count
		global out_count
		global in_count

		if button == pyglet.window.mouse.RIGHT:
			mode += 1
			if mode > 3:
				mode = 0;
			return

		if mode == 0:
		    x_out.append(x)
		    y_out.append(y)
		    if len(x_out) >= 2:
		    	space.add(pymunk.Segment(space.static_body, (x_out[out_count], y_out[out_count]), (x_out[out_count + 1], y_out[out_count + 1]), 1))
		    	out_count += 1
		    	

		if mode == 1:
		    x_in.append(x)
		    y_in.append(y)
		    if len(x_in) >= 2:
		    	space.add(pymunk.Segment(space.static_body, (x_in[in_count], y_in[in_count]), (x_in[in_count + 1], y_in[in_count + 1]), 1))
		    	in_count += 1

		if mode == 2 and button == pyglet.window.mouse.LEFT:
			checkpoints_x_y.append(x)
			checkpoints_x_y.append(y)
			cp_count = 4 * int(int(len(checkpoints_x_y)) / 4 - 1)
			if int(len(checkpoints_x_y)) >= 4:
				checkpoints = pyglet.graphics.vertex_list(2,
					('v2f', (checkpoints_x_y[cp_count], checkpoints_x_y[cp_count + 1], checkpoints_x_y[cp_count + 2], checkpoints_x_y[cp_count + 3])),
					('c3B', (0, 255, 0, 0, 255, 0))
					)

@window.event
def on_mouse_drag(x, y, dx, dy, buttons, modifiers):
	if not start:

	    if mode == 3:
	        player.begin_x = x
	        player.begin_y = y
	        player.begin_rot = player.rotation
	        player.reset()

@window.event
def on_key_press(symbol, modifiers):
	global pressed_keys
	global epsilon
	global x_out, x_in, y_out, y_in
	global checkpoints_x_y
	global checkpoints
	global load
	global first
	global start, cp_count

	if symbol == key.P:
		print("Weights: " + str(agent.model.get_weights()))
		return

	if symbol == key.O:
		start = 0

	if symbol == key.ENTER:
		start = 1
		if first:
			cp_count = 0
			first = False
		return

	if symbol == key.Z:
		agent.model.save(f'models/{MODEL_NAME}__forced_save_{episode}_{int(time.time())}.model')
	if symbol == key.T:
		np.save('track_x_out', np.array(x_out))
		np.save('track_y_out', np.array(y_out))
		np.save('track_x_in', np.array(x_in))
		np.save('track_y_in', np.array(y_in))
		np.save('track_checkpoints_x_y', np.array(checkpoints_x_y))
	if symbol == key.L and not load:
		path = os.path.join(os.getcwd())
		x_out = np.load(file = path + "/track_x_out.npy")
		y_out = np.load(file = path + "/track_y_out.npy")
		x_in = np.load(file = path + "/track_x_in.npy")
		y_in = np.load(file = path + "/track_y_in.npy")
		checkpoints_x_y = np.load(file = path + "/track_checkpoints_x_y.npy")
		for i in range(len(x_out) - 1):
			space.add(pymunk.Segment(space.static_body, (x_out[i], y_out[i]), (x_out[i + 1], y_out[i + 1]), 1))
		for i in range(len(x_in) - 1):
			space.add(pymunk.Segment(space.static_body, (x_in[i], y_in[i]), (x_in[i + 1], y_in[i + 1]), 1))
		checkpoints = pyglet.graphics.vertex_list(2,
				('v2f', (checkpoints_x_y[0], checkpoints_x_y[1], checkpoints_x_y[2], checkpoints_x_y[3])),
				('c3B', (0, 255, 0, 0, 255, 0))
				)
		load = True
		return

	if symbol == key.DOWN:
		epsilon -= 0.05
		return

	if symbol == key.UP:
		epsilon += 0.05
		return

	if symbol in pressed_keys:
		return

	pressed_keys.append(symbol)

@window.event
def on_key_release(symbol, modifiers):
    global pressed_keys

    if symbol in pressed_keys:
        pressed_keys.remove(symbol)		

@window.event
def on_draw():
	window.clear()
	space.debug_draw(options)
	draw()

def main():
    pyglet.clock.schedule_interval(update, 1 / 120)
    pyglet.app.run()


main()